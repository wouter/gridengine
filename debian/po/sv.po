# translation of gridengine.po to swedish
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Bagge <martin.bagge@bthstudent.se>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: gridengine\n"
"Report-Msgid-Bugs-To: gridengine@packages.debian.org\n"
"POT-Creation-Date: 2008-07-11 21:54+0100\n"
"PO-Revision-Date: 2008-06-28 15:22+0200\n"
"Last-Translator: Martin Bagge <martin.bagge@bthstudent.se>\n"
"Language-Team: swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: boolean
#. Description
#: ../gridengine-client.templates:1001 ../gridengine-common.templates:1001
#: ../gridengine-exec.templates:1001 ../gridengine-master.templates:1001
#: ../gridengine-qmon.templates:1001 ../gridengine.templates:1001
msgid "Configure SGE automatically?"
msgstr "Vill du att paketet ställer in SGE automatiskt?"

#. Type: boolean
#. Description
#: ../gridengine-client.templates:1001 ../gridengine-common.templates:1001
#: ../gridengine-exec.templates:1001 ../gridengine-master.templates:1001
#: ../gridengine-qmon.templates:1001 ../gridengine.templates:1001
msgid ""
"Please choose whether you wish to configure SGE automatically (with "
"debconf). If you do not configure it automatically, the daemons or client "
"programs will not work until a manual configuration is performed."
msgstr ""
"Ange om du vill att SGE ska ställas in automatiskt (genom debconf) eller om "
"du vill göra det manuellt vid ett senare tillfälle. HJälpprocesserna och "
"klientprogrammen fungerar inte förrens konfigurationen är utförd."

#. Type: string
#. Description
#: ../gridengine-client.templates:2001 ../gridengine-common.templates:2001
#: ../gridengine-exec.templates:2001 ../gridengine-master.templates:2001
#: ../gridengine-qmon.templates:2001 ../gridengine.templates:2001
msgid "SGE cell name:"
msgstr "Namn på SGE-cellen:"

#. Type: string
#. Description
#: ../gridengine-client.templates:2001 ../gridengine-common.templates:2001
#: ../gridengine-exec.templates:2001 ../gridengine-master.templates:2001
#: ../gridengine-qmon.templates:2001 ../gridengine.templates:2001
msgid ""
"Please provide the SGE cell name for use by client programs and the "
"execution daemon."
msgstr "Ange namnet för SGE-cellen som klientprogrammen ska använda."

#. Type: string
#. Description
#: ../gridengine-client.templates:3001 ../gridengine-common.templates:3001
#: ../gridengine-exec.templates:3001 ../gridengine-master.templates:3001
#: ../gridengine-qmon.templates:3001 ../gridengine.templates:3001
msgid "SGE master hostname:"
msgstr "Värdnamn för huvud-SGE:n"

#. Type: string
#. Description
#: ../gridengine-client.templates:3001 ../gridengine-common.templates:3001
#: ../gridengine-exec.templates:3001 ../gridengine-master.templates:3001
#: ../gridengine-qmon.templates:3001 ../gridengine.templates:3001
msgid ""
"The execution daemon and the client programs need to know where the cluster "
"master is in order to run."
msgstr ""
"Klientprogrammen måste kunna lokalisera klustretes huvudnod för att fungera."

#. Type: string
#. Description
#: ../gridengine-client.templates:3001 ../gridengine-common.templates:3001
#: ../gridengine-exec.templates:3001 ../gridengine-master.templates:3001
#: ../gridengine-qmon.templates:3001 ../gridengine.templates:3001
msgid "Please enter the fully qualified domain name of the grid master."
msgstr "Ange komplett värdnamn med domän för huvudvärden."
